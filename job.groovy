multibranchPipelineJob('sample-groove') {
    branchSources {
        git {
            id('11111') // IMPORTANT: use a constant and unique identifier
            remote('git@gitlab.com:jcsalzeber/sample-maven-project.git')
            credentialsId('gitlab')
            includes('JENKINS-*')
        }
    }
    orphanedItemStrategy {
        discardOldItems {
            numToKeep(20)
        }
    }
}
