package com.zenika.sample.sampleprojectmaven;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleProjectMavenApplicationTests {

    @Value("${info.app.name}")
    private String appName;

	@Test
	public void contextLoads() {
	}

	// TP 2 - Exercice 4
	
	@Test
    public void checkAppName() {
	    assertThat(this.appName).isEqualTo("Sample Application");
    }
    
	@Test
    public void alwaysFail() {
	    assert(false);
    }
}
