package com.zenika.sample.sampleprojectmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleProjectMavenApplication {

	public static void main(String[] args) {
        SpringApplication.run(SampleProjectMavenApplication.class, args);  
        System.out.println("Yo, blabla");
	}

}
