pipeline {
  agent {
    kubernetes {
      label 'jdk8'
      yamlFile 'build-pod.yml'
      defaultContainer 'jdk8'
      idleMinutes 30
        }
    }

    options {
      gitLabConnection('Gitlab')
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }

  stages {
    stage('Clean') {
      steps {
        sh "chmod 755 mvnw"  
        sh "./mvnw clean"
            }
        }

    stage('Sonar') {
      steps {
        sh "./mvnw -B sonar:sonar"
            }
        }

    stage('Build') {
      steps {
        sh "./mvnw -B compile -DskipTests"
            }
        }
    
    stage('Test') {
      steps {
          catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                    sh "./mvnw -B verify"
                }
            }
        }
    
    stage('Package') {
      steps {
        sh "./mvnw -B -DskipTests package"
            }
        }

    stage('Deploy') {
        
      steps {
        withCredentials([file(credentialsId: 'nexus-snapshit', variable: 'MAVEN_SETTINGS')
                ]) { 
            sh "./mvnw -B -s $MAVEN_SETTINGS -DskipTests deploy"
                }
            }
        }
    }

  post {
    always {
      archiveArtifacts artifacts: 'target/*.jar', fingerprint: true
      junit "target/surefire-reports/*.xml"
    }
    
    failure {
      updateGitlabCommitStatus name: 'build', state: 'failed'
    }
    success {
      updateGitlabCommitStatus name: 'build', state: 'success'
    }
  }
}
